Directory Structure

Note that in the root directory each `.txt` file named after a cohort is a list of urls to their githubs

```
├── codeup.dev
│   ├── glacier.txt
│   ├── hampton.txt
│   ├── ike.txt
│   ├── joshua.txt
│   ├── kings.txt
│   └── lassen.txt
├── database-exercises
│   ├── glacier.txt
│   ├── hampton.txt
│   ├── ike.txt
│   ├── joshua.txt
│   ├── kings.txt
│   └── lassen.txt
├── php-exercises
│   ├── glacier.txt
│   ├── hampton.txt
│   ├── ike.txt
│   ├── joshua.txt
│   ├── kings.txt
│   └── lassen.txt
└── simple-simon
    ├── glacier.txt
    ├── hampton.txt
    ├── ike.txt
    ├── joshua.txt
    ├── kings.txt
    └── lassen.txt
```

Where each `.txt` file has a each person in the cohort's github repo url for
that project. Each url is on a newline.

**Example**

`codeup.dev/example.txt`

```
https://github.com/student/codeup.dev
https://github.com/somestudent/Codeup-Web-Exercises
https://github.com/anotherstudent/web-exercises
https://github.com/astudent/codeup-exercises
https://github.com/someone/CodeUp-Exercises-Web
```
